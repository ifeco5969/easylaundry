<?php

    include("connection.php");
global $conn;
//    $hashedPassword = password_hash(password, PASSWORD_DEFAULT);

    $fname=$lname=$user=$email=$password=$confirm_password= "";
    $fnameError=$lnameError=$userError=$emailError=$passwordError=$confirm_passwordError= "";

    if(isset($_POST["reg"])){
        function validateFormData($formData){
            $formData = trim(stripslashes(htmlspecialchars($formData)));
            return $formData;

        }
        if(!$_POST['fname']){
                $fnameError = "Please enter your firstName";
            }else {
                $fname = $_POST['fname'];
            }
        if(!$_POST['lname']){
                $lnameError = "Please enter your lastName";
            }else{
                $lname= $_POST['lname'];
            }
        if(!$_POST['user']){
                $userError = "Please enter your userName";
            }else{
                $user= $_POST['user'];
            }
        if(!$_POST['email']){
                $emailError = "Please enter your emailName";
            }else{
                $email= $_POST['email'];
            }
        if(!$_POST['password']){
                $passwordError = "Please enter your password";
            }else{
                $hashedPassword = password_hash('password', PASSWORD_DEFAULT);
                $password= $hashedPassword;
//                $password= $_POST ['password'];
            }
        if(!$_POST['confirm_password']){
                $confirm_passwordError = "Please enter your confirm_password";
            }else{
                $hashedPassword = password_hash('confirm_password', PASSWORD_DEFAULT);
                $confirm_password= $hashedPassword;
//                $confirm_password= $_POST['confirm_password'];
            }

    }



    if($fname && $lname && $user && $email && $password && $confirm_password){

        $query = "INSERT INTO ifeanyi_table (first_name, last_name, username, email, password, sign_up_date)
         VALUES('$fname', '$lname', '$user', '$email', '$password', CURRENT_TIMESTAMP )";


        if(mysqli_query($conn, $query)){
            echo "New Record added";
        }else{
            echo "Error: ". $query ."<br>". mysqli_error($conn);
        }

        header("location: success.php?success=New Record added");
        exit();


    }


//        if(isset($_POST["reg"])){
//            $query =" UPDATE ifeanyi_table SET
//            username = '{user}', password = '{password}',
//             first_name = '{fname}',
//             last_name = '{lname}',
//             email = '{email}',";
//        }



?>



<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create</title>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>


    <body>
    <div id="body">
        <div class="blurry">
            <div class="col-xs-12 col-sm-6 offset-sm-3" style="font-family: Georgia, Time, serif; color: white; padding-top: 50px;">

                <h1 class="text-center">Create an Account</h1>

                <form action="register.php" method="post">
                    <div class="form-group">
                        <label for="fname">First Name</label>
                        <small class="text-danger"><?php echo $fnameError?></small>
                        <input type="text" class="form-control input-new" name="fname" id="fname" placeholder="F/Name">
                    </div>

                    <div class="form-group">
                        <label for="lname">Last Name</label>
                        <small class="text-danger"><?php echo $lnameError?></small>
                        <input type="text" class="form-control input-new"" name="lname" id="lname" placeholder="L/Name">
                    </div>

                    <div class="form-group">
                        <label for="user">Username</label>
                        <small class="text-danger"><?php echo $userError?></small>
                        <input type="text" class="form-control input-new"" name="user" id="user" placeholder="UserName">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <small class="text-danger"><?php echo $emailError?></small>
                        <input type="email" class="form-control input-new"" name="email" id="email" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <small class="text-danger"><?php echo $passwordError?></small>
                        <input type="password" class="form-control input-new"" name="password" id="password" placeholder="Password">
                    </div>

                    <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <small class="text-danger"><?php echo $confirm_passwordError?></small>
                        <input type="password" class="form-control input-new"" name="confirm_password" id="confirm_password" placeholder="Confirm_password">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg" name="reg">Create</button>
                    </div>

                </form>




            </div>
        </div>
    </div>

    <script src="js/bootstrap.js"></script>

    </body>



</html>