<!DOCTYPE html>

<html>

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Table</title>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body>
        <div id="body">
            <div class="blurry">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 offset-sm-3" style="font-family: Georgia, Time, serif; color: white;padding-top: 70px;">
                        <h1 class="text-center">Creating A Price List Table</h1>
                        <table class="table table-bordered book-table" id="myTable" style="margin-top: 50px;">

                            <tr id="item" style="color: white;">
                                <th class="text-center">ITEM ID</th>
<!--                                <th class="text-center">ITEM</th>-->
                                <th class="text-center">NAME</th>
                                <th class="text-center">PRICE</th>
                            </tr>
                    </div>
                    <div class="">
                        <input type="button" class="button add-button my-3" name="add" value="Add" onclick="addition();">
                    </div>
                    </table>
                </div>
                </div>

            </div>
        </div>



    </body>

</html>



