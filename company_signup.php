<?php

include("connection.php");
global $conn;


$name=$email=$address=$num=$password=$confirm_password= "";
$nameError=$emailError=$addressError=$numError=$passwordError=$confirm_passwordError= "";

    if(isset($_POST["create"])) {
        function validateFormData($formData)
        {
            $formData = trim(stripslashes(htmlspecialchars($formData)));
            return $formData;

        }

        if (!$_POST['name']) {
            $nameError = "Please enter Company Name";
        } else {
            $name = $_POST['name'];
        }
        if (!$_POST['email']) {
            $emailError = "Please enter Company Email";
        } else {
            $email = $_POST['email'];
        }
        if (!$_POST['address']) {
            $addressError = "Please enter Company address";
        } else {
            $address = $_POST['address'];
        }
        if (!$_POST['num']) {
            $numError = "Please enter Company phone Number";
        } else {
            $num = $_POST['num'];
        }
        if(!$_POST['password']){
            $passwordError = "Please enter password";
        }else{
            $hashedPassword = password_hash('password', PASSWORD_DEFAULT);
            $password= $hashedPassword;
//                $password= $_POST ['password'];
        }
        if(!$_POST['confirm_password']){
            $confirm_passwordError = "Please enter confirm_password";
        }else{
            $hashedPassword = password_hash('confirm_password', PASSWORD_DEFAULT);
            $confirm_password= $hashedPassword;
//                $confirm_password= $_POST['confirm_password'];
        }
    }

    if($name && $email && $address && $num && $password && $confirm_password){
        $query = "INSERT INTO companies_name (name, email, address, phone_number, password, sign_up_date)
         VALUES('$name', '$email', '$address', '$num', '$password', CURRENT_TIMESTAMP )";

        if(mysqli_query($conn, $query)){
            echo "New Record added";
        }else{
            echo "Error: ". $query ."<br>". mysqli_error($conn);
        }
        header("location: success.php?success=New Record added");
        exit();
    }
?>




<!DOCTYPE html>

<html>

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title>Company page</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">

    </head>

    <body>
    <div id="body">
        <div class="blurry">
            <div class="col-xs-12 col-sm-6 offset-sm-3" style="font-family: Georgia, Time, serif; color: white; padding-top: 50px;">

                <h1 class="text-center">Create a Company Account</h1>

                <form action="#" method="post">
                    <div class="form-group">
                        <label for="name">Company Name</label>
                        <small class="text-danger"><?php echo $nameError?></small>
                        <input type="text" class="form-control input-new" name="name" id="name" placeholder="C/Name">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <small class="text-danger"><?php echo $emailError?></small>
                        <input type="text" class="form-control input-new" name="email" id="email" placeholder="EMAIL">
                    </div>

                    <div class="form-group">
                        <label for="num">Phone Number</label>
                        <small class="text-danger"><?php echo $numError?></small>
                        <input type="text" class="form-control input-new" name="num" id="num" placeholder="NUMBER">
                    </div>

                    <div class="form-group">
                        <label for="address">Company Address</label>
                        <small class="text-danger"><?php echo $addressError?></small>
                        <textarea class="form-control input-new" name="address" id="address" placeholder="ADDRESS"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <small class="text-danger"><?php echo $passwordError?></small>
                        <input type="password" class="form-control input-new"" name="password" id="password" placeholder="PASSWORD">
                    </div>

                    <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <small class="text-danger"><?php echo $confirm_passwordError?></small>
                        <input type="password" class="form-control input-new"" name="confirm_password" id="confirm_password" placeholder="CONFIRM_PASSWORD">
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg" name="create">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    </body>

</html>