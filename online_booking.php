<?php

include('connection.php');
global $conn;
$query = "select * from pricelist";
$result = $conn->query($query);
$pick=[];
$totalPrice=$totalItems=0;
$name = $email = $num = $address = $company = $digit = "";
$nameError = $emailError = $numError = $addressError = $companyError = $pickError = $digitError = "";
//var_dump($_POST);

if (isset($_POST["booking"])) {
    function validateFormData($formData)
    {
        $formData = trim(stripslashes(htmlspecialchars($formData)));
        return $formData;
    }

    if (!$_POST['name']) {
        $nameError = "Please enter your fullName";
    } else {
        $name = $_POST['name'];
    }
    if (!$_POST['email']) {
        $emailError = "Please enter your Email";
    } else {
        $email = $_POST['email'];
    }
    if (!$_POST['num']) {
        $numError = "Please enter your Phone number";
    } else {
        $num = $_POST['num'];
    }
    if (!$_POST['address']) {
        $addressError = "Please enter your Address";
    } else {
        $address = $_POST['address'];
    }
    if (!$_POST['company']) {
        $companyError = "Please select company of your choice";
    } else {
        $company = $_POST['company'];
    }
    if (!$_POST['pick_name']) {
        $pickError = "Select an item";
    } else {
        foreach ($_POST['pick_name'] as $key => $value) {
            $pick[]=['id'=>$_POST['pick_id'][$key],'name'=>$value,'qty'=>$_POST['pick_number'][$key],'total'=>$_POST['pick_price'][$key]];
            $totalPrice += $_POST['pick_price'][$key];
            $totalItems += $_POST['pick_number'][$key];
        }
    }
}


if ($name && $email && $num && $address && $company) {

    $query = "INSERT INTO Bookings_table (full_name, email, phone_number, address, recommended_company, items, total_items,total_price, sign_up_date)
         VALUES('$name', '$email', '$num', '$address', '$company', '".json_encode($pick)."', '$totalItems','$totalPrice', CURRENT_TIMESTAMP )";


    if (mysqli_query($conn, $query)) {
        echo "New Record booking added";
        header("location: added_booking.php?added=New Record booking added");
    } else {
        echo "Error: " . $query . "<br>" . mysqli_error($conn);
    }

}

?>


<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Booking</title>

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
<div id="body">
    <div class="blurry">
        <div class="row">
            <div class="col-xs-12 col-sm-6 offset-sm-3" style="font-family: Georgia, Time, serif; color: white;padding-top: 70px;">
                <h1 class="text-center">Online Booking</h1>

                <form id="formAddName" action="online_booking.php" method="post">
                    <div class="form-group">
                        <label for="name">Full Name</label>
                        <small class="text-danger"><?php echo $nameError ?></small>
                        <input type="text" class="form-control input-new" name="name" id="name" placeholder="FullName">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <small class="text-danger"><?php echo $emailError ?></small>
                        <input type="email" class="form-control input-new" name="email" id="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="num">Phone Number</label>
                        <small class="text-danger"><?php echo $numError ?></small>
                        <input type="text" class="form-control input-new" name="num" id="num" placeholder="Number">
                    </div>

                    <div class="form-group">
                        <label for="address">Pick Up Address</label>
                        <small class="text-danger"><?php echo $addressError ?></small>
                        <textarea class="form-control input-new" name="address" id="address" placeholder="Address"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="company">Recommended Company</label>
                        <small class="text-danger"><?php echo $companyError ?></small>
                        <select class="form-control input-new" name="company" id="company">
                            <option>EasyLaundry</option>
                        </select>
                    </div>

                    <div class="container vertical">

                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label for="pick">Categories of Item</label>
                                    <small class="text-danger"><?php echo $pickError ?></small>
                                    <select name="pick" id="pick" class="custom-select input-new1">

                                        <?php
                                        while ($row = $result->fetch_assoc()) {
                                            echo '<option value="' . $row['id'] . '" price=' . $row['price'] . '>' . $row['name'] . '</option>';
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label for="digit">Number</label><br>
                                    <!--<small class="text-danger">-->
                                    <?php //echo $digitError?><!--</small>-->
                                    <input class="number-select input-new" type="number" name="digit" id="digit" min="1"
                                           max="500" step="1">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group" style="margin:auto; padding: 30px">
                                    <!--<button class="btn btn-default add-button" name="add" onclick="addition()">Add</button>-->
                                    <input type="button" class="button add-button" name="add" value="Add" onclick="addition();"></div>
                            </div>
                        </div>

                        <table class="table table-bordered book-table" id="myTable">

                            <tr id="item" style="color: white;">
                                <th class="text-center">ITEM ID</th>
                                <th class="text-center">ITEM</th>
                                <th class="text-center">QTY</th>
                                <th class="text-center">TOTAL</th>
                            </tr>

                        </table>
                        <br>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg" name="booking">Book</button>
                        </div>


                    </div>

            </div>

            </form>

        </div>
    </div>
</div>




<script src="js/bootstrap.js"></script>
<script type="text/javascript">

    function addition() {
        var item = document.getElementById('pick');
        var qty = document.getElementById('digit').value;
        var x = document.getElementById('myTable');
        var selected_item = item.options[item.selectedIndex].innerHTML;
        var selected_id = item.options[item.selectedIndex].value;
        var selected_price = (item.options[item.selectedIndex].getAttribute('price')) * qty;
//            console.log(selected_price);
//            console.log(x);
        var row = x.insertRow(1);
        var s = row.insertCell(0);
        var y = row.insertCell(1);
        var z = row.insertCell(2);
        var t = row.insertCell(3);
        s.innerHTML = "<input class='input-new text-center' type='text' name='pick_id[]' value=" + selected_id + ">";
        y.innerHTML = "<input class='input-new text-center text-truncate' type='text' name='pick_name[]' value=" + selected_item + ">";
        z.innerHTML = "<input class='input-new text-center' type='text' name='pick_number[]' value=" + qty + ">";
        t.innerHTML = "<input class='input-new text-center' type='text' name='pick_price[]' value=" + selected_price + ">";
    }


    //        var western = ["manchester united", "juventus", "real madrid", "paris saint german"];
    //        console.log(western);
    //
    //        document.write('<table class="table table-bordered" style="margin-top: -50px">');
    //        document.write('<tr><th>Items</th><th>Quantity</th></tr>');
    //
    //        for(var i = 0; i < western.length; i++){
    //            var j = i + 1;
    //            document.write('<tr><td>' + western[i] + '</td> <td>'+ j +'</td></tr>');
    //        }
    //
    //        document.write('</table>');


    //        function addition(){
    //            window.alert("I am an alert box!");
    //        }

</script>

</body>
</html>