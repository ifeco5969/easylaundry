<?php
    define("TITLE", "My Web");
?>



<!doctype html>

<html>

    <head>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title><?php echo TITLE;?></title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body>
        <header class="fix">

            <div class="row">
                <div class="easy col-md-3">
                    <a href="#"><img src="img/laundry.png" width="250" height="100"></a>
                </div>
                <div class="text col-md-5 col-xs-12">
                    <a href="#">Home</a>
                    <a href="#features">Features</a>
                    <a href="prices.php">Price List</a>

                    <div class="dropdown">
                        <a href="#" class="dropbtn" style="margin-right: 8px;">Services</a>
                        <div class="dropdown-content">
                            <a href="#">Delivery</a>
                            <a href="#">Pick Up</a>
                            <a href="#">Starching</a>
                            <a href="#">Washing</a>
                            <a href="#">Ironing</a>
                        </div>
                    </div>

                    <a href="#">About</a>
                    <a href="#">Contact Us</a>



                </div>

                <div class="col-md-4 button">
                    <div class="row" style="margin-left:52px;">
                        <div class="btn btn-primary col-md-4 mb-2" role="button">
                            <a class="nav-link" href="question.php" style="color: white;">Register</a>
                        </div>
                        <div class="btn btn-primary mx-2 col-md-4 mb-2" role="button" style="flex:1;">
                            <a class="nav-link" href="login.php" style="color:white;">Login</a>
                        </div>
                    </div>
                </div>

            </div>

        </header>
        <div class="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-6">
                    <div class="log" style="margin-top:40px;">
                        <div class="blur">
                            <div class="foo text-center">
                                <a href="online_booking.php"><button class="btn btn-primary btn-lg">Book Online</button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="cont">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="graph" style="margin-left: 30px; margin-top: 20px; text-align: justify; font-family: Georgia, Time, serif;">
                            <h1>About Us</h1>
                            <p>EasyLaundry.com.ng is a platform designed to handle laundry pick-up and delivery. We are affiliated with several laundry outlets around Gwarimpa and Life camp Abuja. In accordance with our customer’s request, we rapidly pick up laundry from the customer’s abode to the nearest or preferred laundry outlet as specified during their order on our reliable and easy-to-use online platform. Also, we make sure to deliver the clean laundry back to the customer’s doorstep at the specified time range.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="feat" id="features" style="margin-left: 30px; margin-top: 20px; text-align: justify; font-family: Georgia, Time, serif;">
                            <h1>Features</h1>
                            <ul style="list-style-type: none;">
                                <li><strong>Book Online</strong></li>
                            </ul>
                            <p>This is the button that directs you to the point where you request for a pick up and the admin of the site/platform receives it. In circumstances where the laundry company has provisions for pick up and deliveries, they will get the request directly but if vice versa, the order is been received by EasyLaundry.com.ng. In the former, the Laundry outlet realizes the delivery fee and 25% of the laundry fee is been given to EasyLaundry.com.ng but in the latter, both the delivery fee and 25% of the laundry fee is been realized by EasyLaundry.com.ng.</p>
                        </div>
                    </div>

                </div>

            </div>


            <div id="container">
                <div class="num text-center" style="padding-top: 80px;">
                    <h3>Easy as 1-2-3</h3>
                </div>
                <div class="steps row">
                    <div class="col-md-4 col-xs-12 offset-sm-0">
                        <h4>Step 1</h4>
                        <p>Schedule a pickup for now, later, or simply leave your clothes with your doorman, by using the book online button. We’re easy, to do whatever works for you.</p>
                    </div>
                    <div class="col-md-4 col-xs-12 offset-sm-0">
                        <h4>Step 2</h4>
                        <p>A professional Cleanly concierge will swing by with custom laundry and garment bags to collect your items—so your clothes are protected</p>
                    </div>
                    <div class="col-md-4 col-xs-12 offset-sm-0">
                        <h4>Step 3</h4>
                        <p>Your clothes are returned fresh and folded 72 hours later. Meanwhile, you can relax with a cup of joe (or herbal tea, if that’s your thing). </p>
                    </div>
                </div>
            </div>

            <footer id="footer">
                <div class="foot row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <a href="#"><img src="img/laundry.png" height="80" width="150"></a>
                    </div>
                    <p style="padding-right: 180px;"><span>Copyright &copy; 2018 &mdash; <a href="#" class="laund">Easy Laundry</a></span></p>
                    <a href="#"><p><span>Terms & Conditions/Privacy Policy/FAQ</span></p></a>
                </div>

            </footer>

        </div>




    <script src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
    </body>



</html>