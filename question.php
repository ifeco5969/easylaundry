<!DOCTYPE html>

<html>

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title>Sign up page</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">

    </head>

    <body>
    <div id="body">
        <div class="blurry">
            <h1 class="text-center" style="font-family: Georgia, Time, serif; color: white;">SIGN UP AS</h1>
            <div class="fluid text-center" style="display: flex; font-family: Georgia, Time, serif; margin-top: 70px;">
                <div class="company" style="flex: 1">
                    <h2 style="color: white">Company</h2>
                    <button type="button" class="btn btn-primary btn-lg my-5"><a href="company_signup.php">Sign up</a></button>
                </div>
                <div class="line"></div>
                <div class="customer" style="flex: 1">
                    <h2 style="color: white;">Customer</h2>
                    <button type="button" class="btn btn-secondary btn-lg my-5"><a href="register.php">Sign up</a></button>
                </div>
            </div>
        </div>
    </div>


        <style>
            .line {
                border-left: 2px solid #d2d2d2;
                height: 500px;
                left: 50%;
            }
            a {
                color: white;
            }
        </style>



    </body>

</html>