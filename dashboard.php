<!doctype html>

<html>

    <head>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title>DashBoard</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body>

    <header class="fix">

        <div class="row">
            <div class="easy col-md-3">
                <a href="#"><img src="img/laundry.png" width="250" height="100"></a>
            </div>
            <div class="text col-md-5 col-xs-12">
                <a href="#">Home</a>
                <a href="#">Features</a>
                <a href="prices.php">Price List</a>

                <div class="dropdown">
                    <a href="#" class="dropbtn" style="margin-right: 8px;">Services</a>
                    <div class="dropdown-content">
                        <a href="#">Delivery</a>
                        <a href="#">Pick Up</a>
                        <a href="#">Starching</a>
                        <a href="#">Washing</a>
                        <a href="#">Ironing</a>
                    </div>
                </div>

                <a href="#">About</a>
                <a href="#">Contact Us</a>

            </div>

            <div class="col-md-4 but">
                <div class="row" style="margin-left:160px;">
                    <div class="btn btn-primary col-md-4 col-xs-6 mb-2" role="button">
                        <a class="nav-link" href="register.php" style="color: white;">Support</a>
                    </div>
                    <div class="btn btn-primary mx-2 col-md-4 col-xs-6 mb-2" role="button">
                        <a class="nav-link" href="login.php" style="color:white;">Log out</a>
                    </div>
                </div>
            </div>

        </div>

    </header>

<!--    <div class="ground" style="background-size: cover">-->
<!--        <div class="kol">-->
<!--            <div class="row">-->
<!--                <div class="col-md-12">-->
<!--                    <a><img src="img/pix.jpg" width="1530" height="500"></a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    </div>-->

    </body>


</html>