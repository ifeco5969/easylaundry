<!DOCTYPE html>

<html>

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Profile Update</title>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body>
    <div id="body">
        <div class="blurry">
            <div class="col-xs-12 col-sm-6 offset-sm-3" style="font-family: Georgia, Time, serif; color: white; padding-top: 50px;">

            <h1>UpDate Info</h1>

            <form action="#" method="post">
                <div class="form-group">
                    <label for="name">Company Name</label>
        <!--            <small class="text-danger">--><?php //echo $nameError?><!--</small>-->
                    <input type="text" class="form-control input-new" name="name" id="name" placeholder="C/Name">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
        <!--            <small class="text-danger">--><?php //echo $emailError?><!--</small>-->
                    <input type="text" class="form-control input-new" name="email" id="email" placeholder="EMAIL">
                </div>

                <div class="form-group">
                    <label for="num">Phone Number</label>
        <!--            <small class="text-danger">--><?php //echo $numError?><!--</small>-->
                    <input type="text" class="form-control input-new" name="num" id="num" placeholder="NUMBER">
                </div>

                <div class="form-group">
                    <label for="address">Company Address</label>
        <!--            <small class="text-danger">--><?php //echo $addressError?><!--</small>-->
                    <textarea class="form-control input-new" name="address" id="address" placeholder="ADDRESS"></textarea>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
        <!--            <small class="text-danger">--><?php //echo $passwordError?><!--</small>-->
                    <input type="password" class="form-control input-new"" name="password" id="password" placeholder="PASSWORD">
                </div>

                <div class="form-group">
                    <label for="confirm_password">Confirm Password</label>
        <!--            <small class="text-danger">--><?php //echo $confirm_passwordError?><!--</small>-->
                    <input type="password" class="form-control input-new"" name="confirm_password" id="confirm_password" placeholder="CONFIRM_PASSWORD">
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg" name="create">Update</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    </body>

</html>
