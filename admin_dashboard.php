<!doctype html>

<html>

    <head>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title>DashBoard</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
        <link rel="stylesheet" href="css/dashboard.css">
        <link rel="stylesheet" href="css/content.css">
        <link rel="stylesheet" href="css/all.css">

    </head>

    <body>
        <section id="menu">
            <h2 class="head" id="name">Welcome Admin</h2>
            <!--<h4 class="head" id="name">Welcome</h4>-->
            <nav>
                <a href="#" class="active"><i class="fa" aria-hidden="true"><img src="img/home.svg" alt=""></i>Home</a>
                <!--<a href="#"><i class="fa" aria-hidden="true"><img src="img/writing.svg"></i>Note</a>-->
                <!--<a href="#"><i class="fa" aria-hidden="true"><img src="img/book.svg"></i>Book</a>-->
                <!--<a href="#"><i class="fa" aria-hidden="true"><img src="img/calendar.svg"></i>Calendar</a>-->
                <a href="update_profile.php"><i class="fa" aria-hidden="true"><img src="img/user.svg"></i>User</a>
                <a href="#"><i class="fa" aria-hidden="true"><img src="img/info.svg"></i>UpDate Info</a>
                <a href="#"><i class="fa" aria-hidden="true"><img src="img/delivery.svg" width="50" height="50"></i>Delivery</a>
                <a href="#"><i class="fa" aria-hidden="true"><img src="img/cog.svg"></i>Settings</a>
            </nav>
        </section>

        <header class="row">
            <div class=" col-md-2 heading" style="margin-left: 390px;">
                <h1>Dashboard</h1>
                <p>Welcome to EasyLaundry Dashboard</p>
            </div>
            <div class="col-md-2" style="margin-left: auto;">
            </div>
            <div class="col-md-4">
                <ul class="nav justify-content-end add">
                    <li class="nav-item">
                        <a class="btn btn-primary" role="button"" href="table.php">Create Table</a>
                        <a class="btn btn-primary" role="button"" href="#">LogOut</a>
                    </li>
                </ul>
            </div>
        </header>

            <section id="content">
                <div class="cards">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="img">
                                <div class="user-img"><img src="img/laundry.png"></div>
                                <div class="user-name">EasyLaundry Nigeria Limited</div>
                                <!--                            <div class="user-title">Full Web Developer</div>-->
                                <hr>
                                <div class=" school col-md-12">
                                    <div class="education">Company Name: EasyLaundry</div>
                                </div>
                                <div class="school col-md-12">
                                    <div class="schools">Email: easylaundry@gmail.com</div>
                                    <div class="schools">Phone: 08074772829</div>
                                    <div class="schools">Address: lekki, Lagos</div>
                                    <div class="schools">about: Delivery services on our platform works based on the registered laundry outlets. In cases </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="position: absolute";>
                        <div class="card int">
                            <h6>Interview Schedule</h6>
                            <div class="date">Nov 14, 2018</div>
                            <div class="school col-md-2">
                                <div class="job-type">IT</div>
                            </div>
                            <div class="col-md-10 branch">
                                <div class="schools">Best Buy HQ</div>
                                <div class="schools">Senior developer</div>
                                <div class="time">4:30 pm</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="position: absolute; left: 1120px;">
                        <div class="card int">
                            <h6>Interview Schedule</h6>
                            <div class="date">Nov 14, 2018</div>
                            <div class="school col-md-2">
                                <div class="job-type">IT</div>
                            </div>
                            <div class="col-md-10 branch">
                                <div class="schools">Best Buy HQ</div>
                                <div class="schools">Senior developer</div>
                                <div class="time">4:30 pm</div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>


<!--            <table class="table table-bordered">-->
<!--                <thead>-->
<!--                    <tr>-->
<!--                        <th>COMPANY</th>-->
<!--                        <th>JOB</th>-->
<!--                        <th>END OF APPLICATION</th>-->
<!--                        <th>LOCATION</th>-->
<!--                    </tr>-->
<!--                </thead>-->
<!--                <tbody>-->
<!--                    <tr>-->
<!--                        <td>Online Shopping</td>-->
<!--                        <td>PHP Developer</td>-->
<!--                        <td>Nov 15, 2018</td>-->
<!--                        <td>Lekki, Lagos</td>-->
<!--                    </tr>-->
<!--                </tbody>-->
<!--            </table>-->


<!--            <p class="para" style="font-family: Georgia, Time, serif">Delivery services on our platform works based on the registered laundry outlets. In cases where they offer delivery services, we charge 25% of the total service plus the laundry fee. However, in cases when they do not offer delivery services, we take charge of the delivery plus 25% on the laundry fee.-->
<!--                Furthermore, the delivery fee usually range from N1,000 – N2,000 depending on the distance from the customer’s home to the preferred laundry outlet. Therefore, the farther the distance of delivery, the higher the delivery fee. This means that the payment for delivery to a particular outlet is based on the distance from their respective location to the requested laundry outlet. However, when the customer does not request a particular laundry outlet, the charges will be based on the distance between our office and the location of the customer.-->
<!--            </p>-->


    </body>


</html>
