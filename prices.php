<?php

include('connection.php');
global $conn;
$query="select * from pricelist";
$result = $conn->query($query);
?>
<!DOCTYPE html>


<html>

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Price Table</title>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body>
        <div id="body">
            <div class="blurry" style="height:1100px; padding-top: 20px;">
                <h1 class="text-center" style="color:white;">Company Price Lists</h1>

                <div class="row" style="margin-left: 500px;">
                <div class="col-xs-12 col-sm-6">
                <form>
                    <div class="form-group">
<!--                        <label for="comp" style="color: white;margin-left: 170px">Companies Name</label>-->
                        <select class="form-control" name="comp" id="comp" onchange="myFunction(this)">
                            <option value="1">IFEX INTERNATIONAL LIMITED</option>
                        </select>
                    </div>
                </form>
                </div>
                </div>
                <div class="table-responsive-xl">
                    <table class="table table-bordered">
                        <thead style="color:white;">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Items</th>
                            <th scope="col">Prices</th>
                        </tr>
                        <tbody style="color: white">
                        <?php
                        while($row = $result->fetch_assoc()){
                            echo '<tr>
                            <th scope="row">'.$row['id'].'</th>
                            <td>'.$row['name'].'</td>
                            <td>&#8358;'.$row['price'].'</td>
                        </tr>';
                        }
                        ?>

                </div>
            </div>
        </div>
        <script src="js/bootstrap.js"></script>
        <script type="text/javascript">
//            function myFunction() {
//               var d = document.getElementById("comp").value;
//                alert(d);
//            }
        function myFunction(a) {
            var x = (a.value || a.options[a.selectedIndex].value);  //crossbrowser solution =)
            alert(x);
        }
        </script>

    </body>


</html>