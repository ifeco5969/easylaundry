<!doctype html>

<html>

    <head>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title>DashBoard</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
        <link rel="stylesheet" href="css/dashboard.css">
        <link rel="stylesheet" href="css/content.css">
        <link rel="stylesheet" href="css/campdash.css">
        <link rel="stylesheet" href="css/all.css">

    </head>

    <body>
    <section id="menu">
        <h2 class="head" id="name">Welcome Ifex Int'l Limited</h2>
        <!--<h4 class="head" id="name">Welcome</h4>-->
        <nav>
            <a href="#" class="active"><i class="fa" aria-hidden="true"><img src="img/home.svg" alt=""></i>Home</a>
            <!--<a href="#"><i class="fa" aria-hidden="true"><img src="img/writing.svg"></i>Note</a>-->
            <!--<a href="#"><i class="fa" aria-hidden="true"><img src="img/book.svg"></i>Book</a>-->
            <!--<a href="#"><i class="fa" aria-hidden="true"><img src="img/calendar.svg"></i>Calendar</a>-->
            <a href="update_profile.php"><i class="fa" aria-hidden="true"><img src="img/user.svg"></i>User</a>
            <a href="#"><i class="fa" aria-hidden="true"><img src="img/info.svg"></i>UpDate Info</a>
            <a href="#"><i class="fa" aria-hidden="true"><img src="img/cog.svg"></i>Settings</a>
        </nav>
    </section>

    <header class="row">
        <div class=" col-md-2 heading" style="margin-left: 445px;">
            <h1>Dashboard</h1>
            <p>Welcome to Ifex Int'l Limited Dashboard</p>
        </div>
        <div class="col-md-2" style="margin-left: auto;">
        </div>
        <div class="col-md-4">
            <ul class="nav justify-content-end add">
                <li class="nav-item">
                    <a class="btn btn-primary" role="button"" href="table.php">Create Table</a>
                    <a class="btn btn-primary" role="button"" href="#">LogOut</a>
                </li>
            </ul>
        </div>
    </header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 box">
                <h2>Maintenance</h2>
                <div class="circle text-center">
                    <p style="font-size: 30px;">100%</p>
                    <p>updated</p>
                </div>
            </div>
            <div class="col-md-4 box1">
                <h2>Statistics</h2>
<!--                <p>updated</p>-->
                <div class="lines"></div>
            </div>
        </div>
    </div>

    <div class="info" style="margin-left: 445px; padding-top: 50px; color: purple;">
        <h1>Company Information</h1>
        <p>Company Name: Ifex Int'l Limited</p>
        <p>Address: Goodwill Street, lekki, Lagos.</p>
        <p>Phone Number: 08064504277</p>
        <p>Email: amahifeanyi@gmail.com</p>
    </div>


    </body>

</html>